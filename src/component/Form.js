import React, {Component} from 'react'
import {storage} from '../firebase.js'
import mypic from '../image/article.jpg'


class Form extends Component {
    constructor(props){
        super(props)
        const user = this.props.user || {}
        this.state = {
            name: user.name || '',
            lastname: user.lastname || '',
            access: user.access || '',
            image: user.image || mypic
        }

        this.handleInput = this.handleInput.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.fileSelectedHandler = this.fileSelectedHandler.bind(this)

    }

    handleInput(event){
        const { value, name} = event.target
        this.setState({
            [name]: value
        })
    }

    handleSubmit(event){
      event.preventDefault()
      this.props.onAdd(this.state)
      this.setState({
          name: '',
          lastname: '',
          access: '',
          image: mypic
      })
    }

    async fileSelectedHandler(event){
      if(event.target.files[0]){
        const file = event.target.files[0]
        const snapshot = await storage.ref(`images/${file.name}`).put(file)
        const downloadURL = await snapshot.ref.getDownloadURL()
        this.setState({image: downloadURL})
      }
    }

    render(){
        return(
            <div className="card-form">
              <img className="img" src={this.state.image} alt="you"/>
                <form className="form" onSubmit={this.handleSubmit}>
                    <div className="input">
                        <input
                            type="text"
                            name="name"
                            placeholder="Nombre"
                            onChange={this.handleInput}
                            value={this.state.name}
                        />
                    </div>
                    <div className="input">
                        <input
                            type="text"
                            name="lastname"
                            placeholder="Apellidos"
                            onChange={this.handleInput}
                            value={this.state.lastname}
                        />
                    </div>
                    <div className="select">
                        <select className="button"
                            name="access"
                            onChange={this.handleInput}
                            value={this.state.access}
                            >
                            <option>¿Acceso?</option>
                            <option className="badge badge-pill badge-success ml-2">Si</option>
                            <option className="badge badge-pill badge-danger ml-2">No</option>
                        </select>
                        <input  className="button" type="file" onChange={this.fileSelectedHandler}/>
                        <button  className="button" type="submit">
                            Añadir
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}
export default Form