import React, {Component} from 'react'
import data from '../firebase.js'
import Form from './Form.js'


class Card  extends Component { 
    constructor(props) {
        super(props)
        this.state =  {
          isEditionMode: false
        }
    }

  render() {
    if(this.state.isEditionMode){
      return (<Form user={this.props.user} onAdd={this.onEdit.bind(this)}/>)
    }
    return(
      <div className="card-form user">
          <img className="img" src={this.props.user.image} alt="you" />
            <div className="user-data">
              <div>
                  <p>{this.props.user.name}  {this.props.user.lastname}</p>
              </div>
              <div>
                  Acceso:{this.props.user.access}
              </div>
              <div className="icons">
                  <span className="material-icons" onClick={()=>this.handleRemove()}>
                  cancel
                  </span>
                  <i className="material-icons" onClick={()=>this.handleEdit()}>
                  create
                  </i>
              </div>
          </div>
      </div>
      )
  }

  handleRemove() {
      if(window.confirm ('¿Estás seguro de querer borrar usuario?')){
        data.collection('user').doc(this.props.user.id).delete()
      }
  }
      
  handleEdit() {
    this.setState({isEditionMode: true})
    }

  onEdit(user) {
    this.setState({isEditionMode: false})
    data.collection("user").doc(this.props.user.id).set({
      name: user.name,
      lastname: user.lastname,
      access: user.access,
      image: user.image
    })
    .then(function() {
        console.log("Document successfully written!")
    })
    .catch(function(error) {
        console.error("Error writing document: ", error)
    })
  }
    
}

export default Card