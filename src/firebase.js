import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'

let firebaseConfig = {
   apiKey: "AIzaSyA2XfgFdO6DyIg4qFN3_e0oMnnYvQo3Ul4",
   authDomain: "seguridad-s-l.firebaseapp.com",
   databaseURL: "https://seguridad-s-l.firebaseio.com",
   projectId: "seguridad-s-l",
   storageBucket: "seguridad-s-l.appspot.com",
   messagingSenderId: "611288272829",
   appId: "1:748952278839:web:ff605991215b12a5baecb2"
 }

 const fb = firebase.initializeApp(firebaseConfig)

export const data = fb.firestore()

const storage = fb.storage()

export {
   storage, data as default
}
